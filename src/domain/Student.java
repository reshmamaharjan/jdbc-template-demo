package domain;

/**
 * @Author Reshma
 * @Date 7/4/2018
 * @Month 07
 **/
public class Student {
    public Student() {

    }

    public int getSid() {
        return sid;
    }

    public Student(String sname, String address) {
        this.sname = sname;
        this.address = address;
    }


    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private int sid;
    private String sname;
    private String address;

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
