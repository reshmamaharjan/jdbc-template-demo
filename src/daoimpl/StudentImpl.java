package daoimpl;

import domain.Student;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @Author Reshma
 * @Date 7/4/2018
 * @Month 07
 **/
public class StudentImpl {
    DataSource dataSource;
    JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void addStudent(Student student) {
        String sql = "insert into student(sname, address) values(?, ?)";
        jdbcTemplate.update(sql, new Object[]{student.getSname(), student.getAddress()});
    }

    public void displayStudent(int sid) {
        String sql = "select * from student where sid=?";
        Student student = jdbcTemplate.queryForObject(sql, new StudentParameterizedRowMapper(), sid);
        System.out.println("Name="+student.getSname());
        System.out.println("Address="+student.getAddress());
    }

    protected class StudentParameterizedRowMapper implements RowMapper<Student> {

        @Override
        public Student mapRow(ResultSet resultSet, int i) throws SQLException {
            Student s = new Student();
            s.setSid(resultSet.getInt("sid"));
            s.setSname(resultSet.getString("sname"));
            s.setAddress(resultSet.getString("address"));
            return s;
        }
    }
    public void updateStudentName(int sid,String name){
        String sql="Update student set sname=? where sid=? ";
         jdbcTemplate.update(sql,new Object[]{name,sid});
    }
    public void displayAll(){
        String sql="Select * from student";
        System.out.println(jdbcTemplate.query(sql,new StudentParameterizedRowMapper()));

    }

}
