import daoimpl.StudentImpl;
import domain.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author Reshma
 * @Date 7/4/2018
 * @Month 07
 **/
public class Test {
    public static void main(String[] args) {
        ApplicationContext ac=new ClassPathXmlApplicationContext("config/applicationContext.xml");
        StudentImpl student=(StudentImpl) ac.getBean("student");
        /*student.addStudent(new Student("Milon","Gulmi"));
        student.displayStudent(1);
        student.updateStudentName(1,"reshma");
        student.displayStudent(1);*/
       /* student.addStudent(new Student("Ashma","Ktm"));
        student.addStudent(new Student("Meera","Lamjung"));
        student.addStudent(new Student("Sharmila","Terathum"));
        student.addStudent(new Student("Alina","Dharan"));*/
       student.displayAll();


    }
}
